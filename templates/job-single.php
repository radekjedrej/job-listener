<?php include 'inc/header.php'; ?>
  <h2 class="page-header">
  <?= $job->job_title ?> (<?= $job->location ?>)
  </h2>
  <small>Posted By <?= $job->contact_user ?> on <?= $job->post_date ?></small>
  <hr>
  <p class="lead"><?= $job->description ?></p>
  <ul class="list-group">
    <li class="list-group-item">
      <strong>Company: </strong><?= $job->comapny ?>
    </li>
    <li class="list-group-item">
      <strong>Salary: </strong><?= $job->salary ?>
    </li>
    <li class="list-group-item">
      <strong>Contact Email:: </strong><?= $job->contact_email ?>
    </li>
  </ul>
  <br><br>
  <a href="index.php">Go Back</a>
  <br><br>
  <div class="well">
    <a class="btn btn-warning" href="edit.php?id=<?= $job->id ?>">Edit</a>

    <form style="display:inline;" action="job.php" method="POST">
      <input type="hidden" name="del_id" value="<?= $job->id ?>">
      <input type="submit" class="btn btn-danger" value="Delete">
    </form>
  </div>

<?php include 'inc/footer.php'; ?>

