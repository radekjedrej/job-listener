<?php include 'inc/header.php'; ?>
  <h2 class="page-header">Edit Job Listing</h2>
  <form action="edit.php?id=<?= $job->id; ?>" method="POST">
    <div class="form-group">
      <label for="company">Company</label>
      <input type="text" class="form-control" name="company" value="<?= $job->comapny ?>">
    </div>
    <div class="form-group">
      <label for="category">Category</label>
      <select type="text" class="form-control" name="category">
        <option value="0">Choose Category</option>
        <?php foreach($categories as $category) : ?>
          <?php if($job->category_id == $category->id) : ?>
            <option value="<?= $category->id ?>" selected><?= $category->name ?></option>
          <?php else: ?>
            <option value="<?= $category->id ?>"><?= $category->name ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group">
      <label for="job_title">Job Title</label>
      <input type="text" class="form-control" name="job_title" value="<?= $job->job_title ?>">
    </div>
    <div class="form-group">
      <label for="description">Description</label>
      <textarea type="text" class="form-control" name="description"><?= $job->description ?></textarea>
    </div>
    <div class="form-group">
      <label for="location">Location</label>
      <input type="text" class="form-control" name="location" value="<?= $job->location ?>">
    </div>
    <div class="form-group">
      <label for="salary">Salary</label>
      <input type="text" class="form-control" name="salary" value="<?= $job->salary ?>">
    </div>
    <div class="form-group">
      <label for="contact_user">Contact User</label>
      <input type="text" class="form-control" name="contact_user" value="<?= $job->contact_user ?>">
    </div>
    <div class="form-group">
      <label for="contact_email">Contact Email</label>
      <input type="text" class="form-control" name="contact_email" value="<?= $job->contact_email ?>">
    </div>

    <input type="submit" class="btn btn-danger" value="submit" name="submit">
  </form>
  <br>
<?php include 'inc/footer.php'; ?>